﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ05
{
    class Program
    {
       private static Hybrydowa<int> lista;
        public static void wypisywanie()
        {
            Console.WriteLine("Drukuje elementy");
            foreach (int i in lista.ToList<int>())
            {
                Console.WriteLine($"Element: {i}");
            }
        }
        static void Main(string[] args)
        {
           lista = new Hybrydowa<int>();

            lista.Add(5);
            lista.Add(54);
            lista.Add(10);
            lista.Add(3);

            

            Console.WriteLine($"Liczba elementow {lista.Count}");

            wypisywanie();
            Console.WriteLine("Testowanie 2 i 3 kolejka/stos");

            lista.Enqueue(69);

            wypisywanie();

            Console.WriteLine(lista.QueuePeek());

            Console.WriteLine(lista.Dequeue());

            wypisywanie();

            Console.WriteLine("Teraz stos: ");

            lista.Push(58);

            wypisywanie();

            Console.WriteLine(lista.StackPeek());

            Console.WriteLine(lista.Pop());

            wypisywanie();

            Console.WriteLine("Testowanie do statycznej");

            string jedn = "jeden";
            string dwa = "dwa";

            Statyczna.zaminka<string>(ref jedn, ref dwa);

            Console.WriteLine($"Po zamiance jeden:{jedn} dwa:{dwa}");


            int i = 78;
            string lol = "lolo";

            Statyczna.dwaRozne<int, string>(ref i, ref lol);

            Console.WriteLine($"i: {i}  lol: {lol}");


            Console.WriteLine(Statyczna.nowyObiekt<int>());

            Console.WriteLine(Statyczna.zwrocWiekszy<int>(1,1));

            //Zapytac sie czy rozpoznaje sam typ generyczny
            List<int> li=Statyczna.listaPos(1,36,9,5,6,7,2,4,6,2,1);

            foreach (int item in li)
            {
                Console.WriteLine(item);
            }


            Dictionary<klucze,Klas> slo=Statyczna.slownik(new klucze(1), new Klas("Roman", "Oman"));
            Console.WriteLine(slo[new klucze(1)].ToString());

            Dictionary<int,string> dio= new Dictionary<int, string>();
            dio.Add(1, "girata");
            dio.Add(2, "siema");

            Statyczna.elementySlownika<int, string>(dio);


            ICollection lolii=Statyczna.dowolnaIlosc<int>(1, 2, 3, 4, 5, 6);

            if(lolii is Stack<int>)
            {
                Stack<int> stos = (Stack<int>)lolii;
                int ilosc = stos.Count;
                for(int n = 0; n < ilosc; n++)
                {
                    Console.WriteLine(stos.Pop());
                }
            }
            /*
            int[] tab=new int[lista.Count];
            lista.CopyTo(tab, 2);

            foreach(int i in tab)
            {
                Console.WriteLine($"{i}");
            }
            
            Console.WriteLine($"{lista.Contains(10)}");
            Console.WriteLine($"{lista.IndexOf(10)}");
            lista.Insert(100, 69);
            //List<int> lista2 = new List<int>();
            Console.WriteLine("Wypisywanie: ");
            for(int n = 0; n < lista.Count; n++)
            {
                Console.WriteLine($"{n}: {lista[n]}");
            }
            lista.Insert(50 ,70);
            //List<int> lista2 = new List<int>();
            lista.Add(47);
            lista.Add(48);
            Console.WriteLine("Wypisywanie: ");
            for (int n = 0; n < lista.Count; n++)
            {
                Console.WriteLine($"{n}: {lista[n]}");
            }
            Console.WriteLine($"{lista.Count}");

            Console.WriteLine(lista.Remove(70));
            for (int n = 0; n < lista.Count; n++)
            {
                Console.WriteLine($"{n}: {lista[n]}");
            }
            */

            List<GenerycznaKlas<int, string, string, string>> gk = new List<GenerycznaKlas<int, string, string, string>>();
            gk.Add(new GenerycznaKlas<int, string, string, string>(7, "siedem", "siedem", "siedem"));
            gk.Add(new GenerycznaKlas<int, string, string, string>(2, "dwa", "dwa", "dwa"));
            gk.Add(new GenerycznaKlas<int, string, string, string>(1, "jeden", "jeden", "jeden"));
            gk.Add(new GenerycznaKlas<int, string, string, string>(8, "osiem", "osiem", "osiem"));
            GenerycznaKlas<int, string, string, string>[] tabo = new GenerycznaKlas<int, string, string, string>[gk.Count];
            tabo = gk.ToArray();
              Array.Sort(tabo);

            for(int n = 0; n < gk.Count; n++)
            {
                Console.WriteLine(tabo[n].ToString());
            }

            Console.ReadKey();
        }
    }
    struct klucze
    {
        public int Id { get; set; }

        public klucze(int Id)
        {
            this.Id = Id;
        }
    }
    public class Klas
    {
        public string Imie { get; set; }
        public string  Nazwisko { get; set; }

        public Klas(string i, string n )
        {
            this.Imie = i;
            this.Nazwisko = n;
        }
        public override string ToString()
        {
            return $"Imie: {Imie} nazwisko: {Nazwisko}";
        }
    }
}
