﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ05
{
    class GenerycznaKlas<Q,W,E,R> : IComparable,IComparable<GenerycznaKlas<Q,W,E,R>>
        where Q: IComparable
    {
        public Q Id { get; set; }
        public W w { get; set; }
        public E e { get; set; }
        public R r { get; set; }

        public GenerycznaKlas()
        {

        }
        public GenerycznaKlas(Q Id, W w,E e,R r)
        {
            this.Id = Id;
            this.w = w;
            this.r = r;
            this.e = e;
        }

        public override string ToString()
        {
            return $"Id: {Id}, w: {w}, e: {e}, r: {r}";
        }
        public override bool Equals(object obj)
        {
            if(obj== null)
            {
                return false;
            }
            GenerycznaKlas<Q,W,E,R> tmp = obj as GenerycznaKlas<Q, W, E, R>;
            if((System.Object)tmp == null)
            {
                return false;
            }
            return this.Id.Equals(tmp.Id);
            
        }
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

      

        public int CompareTo(GenerycznaKlas<Q, W, E, R> other)
        {
            if(other== null)
            {
                return -1;
            }
            return Id.CompareTo(other.Id);
        }

        public int CompareTo(object obj)
        {
            if(obj.GetType() != this.GetType())
            {
                return -1;
            }
            return CompareTo(obj as GenerycznaKlas<Q, W, E, R>);
        }
    }
}
