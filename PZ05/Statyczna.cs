﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ05
{
    public static class Statyczna
    {
        public static void zaminka<T>(ref T t1, ref T t2)
        {
            T tmp = t1;
            t1 = t2;
            t2 = tmp;
        }

        public static void dwaRozne<T, S>(ref T odin,ref S dwa)
        {
            Console.WriteLine("Odin");
            Console.WriteLine(typeof(T).FullName);
            Console.WriteLine($"Wartosc: {odin}");

            Console.WriteLine("Dwa");
            Console.WriteLine(typeof(S).FullName);
            Console.WriteLine($"Wartosc: {dwa}");

            odin=default(T);
            dwa=default(S);
        }

        public static T nowyObiekt<T>() where T: new()
        {
            return new T();
        }

        public static T zwrocWiekszy<T>(T odin,T dwa) where T: IComparable<T>
        {
            int i=odin.CompareTo(dwa);
            if (i > 0)
            {
                return odin;
            }else if(i< 0)
            {
                return dwa;
            }
            else
            {
                return default(T);
            }
           


        }
        public static List<T> listaPos<T>(params T[] obi) where T: IComparable<T> 
        {
            Array.Sort(obi);
            return obi.ToList<T>();
        }

        public static Dictionary<K,V> slownik<K,V>(K key, V value)
        {
            Dictionary<K,V> kv= new Dictionary<K, V>();
            kv.Add(key, value);
            return kv;
        }

        public static void elementySlownika<T,K>(IDictionary<T,K> test)
        {
            for(int n = 0; n < test.Keys.Count; n++)
            {
                Console.WriteLine(test.Keys.ToList<T>().ElementAt(n) + ": "+test[test.Keys.ToList<T>().ElementAt(n)]);
               
            }
        }

        public static ICollection dowolnaIlosc<T>(params T[] tab)
        {
            if(tab.Length < 3)
            {
                Queue<T> koleja = new Queue<T>();
                foreach(T item in tab)
                {
                    koleja.Enqueue(item);
                }
                return (ICollection)koleja;
            }
            else
            {
                Stack<T> stos = new Stack<T>();
                foreach (T item in tab)
                {
                    stos.Push(item);
                }
                return (ICollection) stos;
            }
        }
    }
}
